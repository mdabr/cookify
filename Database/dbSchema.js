class User {
    /**
     * @param {String} email
     * @param {String} password
     * @param {Date} joined
     * @param {Boolean} isEditor
     * @param {String} name
     * @param {Boolean} isVerified
     */
    constructor(email, password, joined, isEditor, name, isVerified) {
        this.id = new ID();
        this.email = email;
        this.password = password;
        this.joined = joined;
        this.isEditor = isEditor;
        this.name = name;
        this.isVerified = isVerified;
    }
}

class Post {
    /**
     * @param {String} creatorId
     * @param {String} title
     * @param {String} content
     * @param {String} photoId
     */
    constructor(creatorId, title, content, photoId) {
        this.id = new ID();
        this.creatorId = creatorId;
        this.title = title;
        this.content = content;
        this.photoId = photoId;
    }
}

class Course {
    /**
     * @param {String} name
     */
    constructor(name) {
        this.id = new ID();
        this.name = name;
    }
}

class Cuisine {
    /**
     * @param {String} name
     */
    constructor(name) {
        this.id = new ID();
        this.name = name;
    }
}

// Non-collection
class Instruction {
    /**
     * @param {String} content
     * @param {Number} orderNum
     */
    constructor(content, orderNum) {
        this.id = new ID();
        this.content = content;
        this.orderNum = orderNum;
    }
}

// Non-collection
class Ingredient {
    /**
     * @param {String} name
     * @param {String} unit
     * @param {Number} amount
     */
    constructor(name, unit, amount) {
        this.id = new ID();
        this.name = name;
        this.unit = unit;
        this.amount = amount;
    }
}

class Recipe {
    /**
     * @param {String} title
     * @param {Number} cookingTime
     * @param {Number} calories
     * @param {Number} servings
     * @param {String} cuisineId
     * @param {String} courseId
     * @param {String} creatorId
     * @param {String} photoId
     * @param {Array} instructions
     * @param {*} ingredients
     */
    constructor(title, cookingTime, calories, servings, cuisineId, courseId, creatorId, photoId, instructions, ingredients) {
        this.id = new ID();
        this.title = title;
        this.cookingTime = cookingTime;
        this.calories = calories;
        this.servings = servings;
        this.cuisineId = cuisineId;
        this.courseId = courseId;
        this.creatorId = creatorId;
        this.photoId = photoId;
        this.instructions = instructions;
        this.ingredients = ingredients;
    }
}

class Favourite {
    /**
     * @param {String} recipeId
     * @param {String} userId
     */
    constructor(recipeId, userId) {
        this.id = new ID();
        this.recipeId = recipeId;
        this.userId = userId;
    }
}
