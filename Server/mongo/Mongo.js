const mongodb = require('mongodb');

const {
    Courses,
    Cuisines,
    Favourites,
    Recipes,
    Users
} = require('./collections');
const logger = require('../utils/Logger');

const url = 'mongodb://localhost:27017';
const dbName = 'cookify';

/**
 * @type {mongodb.Db}
 */
let db = null;

class Mongo {
    static async connect() {
        if (!db) {
            const client = await new mongodb.MongoClient(url, { useUnifiedTopology: true }).connect();
            db = client.db(dbName);
        }

        logger.info(`Connected to ${url}/${dbName}`);

        return db;
    }

    static async fetchCourses() {
        const courses = await db.collection(Courses).find().toArray();

        return courses.map(course => ({
            name: course.name,
            id: course._id
        }));
    }

    static async fetchCuisines() {
        const cuisines = await db.collection(Cuisines).find().toArray();

        return cuisines.map(cuisine => ({
            name: cuisine.name,
            id: cuisine._id
        }));
    }

    static getFavourites(userId) {
        return db.collection(Favourites).find({ userId }).toArray();
    }

    static getRecipe(recipeId) {
        return db.collection(Recipes).findOne({ _id: recipeId });
    }

    static editFavourite(recipeId, userId, checked) {
        const favourite = { recipeId, userId };

        return checked
            ? db.collection(Favourites).update(favourite, favourite, { upsert: true })
            : db.collection(Favourites).deleteOne({ recipeId, userId });
    }

    static async isFavourite(recipeId, userId) {
        const favourite = { recipeId, userId };

        return (await db.collection(Favourites).findOne(favourite)) !== null;
    }

    static verifyLogin(email, password) {
        return db.collection(Users).findOne({ email, password });
    }

    static getCuisineId(cuisineName) {
        return db.collection(Cuisines).findOne({ name: cuisineName });
    }

    static getCourseId(courseName) {
        return db.collection(Courses).findOne({ name: courseName });
    }

    static addNewRecipe(newRecipe) {
        return db.collection(Recipes).insertOne(newRecipe);
    }

    static fetchRecipe(recipeId) {
        return db.collection(Recipes).findOne({ _id: recipeId });
    }

    static async fetchCourseName(courseId) {
        return (await db.collection(Courses).findOne({ _id: courseId })).name;
    }

    static async fetchCuisineName(cuisineId) {
        return (await db.collection(Cuisines).findOne({ _id: cuisineId })).name;
    }

    static fetchRecipesForCategory(categoryId, categoryType) {
        const types = {
            cuisine: () => db.collection(Recipes).find({ cuisineId: categoryId }).toArray(),
            course: () => db.collection(Recipes).find({ courseId: categoryId }).toArray()
        };

        return (types[categoryType.toLowerCase()])();
    }

    static registerUser(name, email, password) {
        return db.collection(Users).insertOne({ name, email, password, joined: new Date(), isEditor: false, isVerified: false });
    }

    static async isEmailUnique(email) {
        return (await db.collection(Users).find({ email }).toArray()).length === 0;
    }

    static fetchTopRecipes(amount) {
        return db.collection(Recipes).find().sort({ $natural: 1 }).limit(amount).toArray();
    }
}

module.exports = Mongo;
