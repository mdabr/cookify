module.exports = {
    Users: 'Users',
    Posts: 'Posts',
    Courses: 'Courses',
    Cuisines: 'Cuisines',
    Recipes: 'Recipes',
    Favourites: 'Favourites'
};
