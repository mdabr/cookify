const winston = require('winston');

const { createLogger } = winston;
const { combine, simple, printf, colorize } = winston.format;
const { Console } = winston.transports;

module.exports = createLogger({
    level: 'info',
    format: combine(
        simple(),
        printf(({ level, message }) =>
            colorize().colorize(level, message)
        )
    ),
    transports: [
        new Console()
    ]
});
