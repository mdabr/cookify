const express = require('express');
const cors = require('cors');
const fs = require('fs');
const io = require('socket.io')();
const args = require('minimist')(process.argv.slice(2));
const logger = require('./utils/Logger');
const app = express();
const serverPort = args.server || 1337;
const ioPort = args.io || 8000;
const { register, login, cuisines, courses, recipeImage, newRecipe, categories, recipes, recipe, favourite, favourites, slider, isFavourite } = require('./routes');
const { IMAGES } = require('./utils/Constants').URI;
require('./mongo/Mongo').connect();

if (!fs.existsSync(IMAGES)){
    fs.mkdirSync(IMAGES);
}

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

app.use('/register', register);
app.use('/login', login);
app.use('/cuisines', cuisines);
app.use('/courses', courses);
app.use('/recipe-image', recipeImage);
app.use('/new-recipe', newRecipe);
app.use('/categories', categories);
app.use('/recipes', recipes);
app.use('/recipe', recipe);
app.use('/favourite', favourite);
app.use('/favourites', favourites);
app.use('/slider', slider);
app.use('/is-favourite', isFavourite);

logger.info('You can set custom ports:');
logger.info('Server: --server <port>');
logger.info('Socket.io: --io <port>');
app.listen(serverPort, () => logger.info(`App listening on port ${serverPort}!`));
io.listen(ioPort); logger.info(`IO listening on port ${ioPort}`);