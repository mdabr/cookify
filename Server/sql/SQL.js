const sql = require('mssql/msnodesqlv8');

let connection = null;
const config = {
    driver: 'msnodesqlv8',
    connectionString: 'Driver={SQL Server Native Client 11.0};Server={db-mssql};Database={S14199};Trusted_Connection={yes}'
};

class SQL {
    static connect() {
        if (!connection) {
            connection = sql.connect(config);
        }
        return connection;
    }

    static sendQuery(query) {
        return connection.then(pool => pool.request().query(query)).catch(err => {
            throw err;
        });
    }

    static insertUser(...values) {
        const [NAME, EMAIL, PASSWORD, JOINED, ISEDITOR, ISVERIFIED] = values;
        const query = `INSERT INTO USR (UNAME, EMAIL, PASSWRD, JOINED, ISEDITOR, ISVERIFIED) VALUES ('${NAME}', '${EMAIL}', '${PASSWORD}', ${JOINED}, ${ISEDITOR}, ${ISVERIFIED})`;
        return SQL.sendQuery(query);
    }

    static verifyLogin(...values) {
        const [EMAIL, PASSWORD] = values;
        const query = `SELECT USRID, UNAME, ISEDITOR, ISVERIFIED FROM USR WHERE EMAIL='${EMAIL}' AND PASSWRD='${PASSWORD}'`;
        return SQL.sendQuery(query);
    }

    static fetchCuisines() {
        const query = 'SELECT CUISINEID, CNAME FROM CUISINE';
        return SQL.sendQuery(query);
    }

    static fetchCourses() {
        const query = 'SELECT COURSEID, CNAME FROM COURSE';
        return SQL.sendQuery(query);
    }

    static getCuisineId(cuisineName) {
        const query = `SELECT CUISINEID FROM CUISINE WHERE CNAME='${cuisineName}'`;
        return SQL.sendQuery(query);
    }

    static getCourseId(courseName) {
        const query = `SELECT COURSEID FROM COURSE WHERE CNAME='${courseName}'`;
        return SQL.sendQuery(query);
    }

    static addNewRecipe(USRID, { TITLE, COOKING_TIME, CALORIES, SERVINGS, CUISINEID, COURSEID, PHOTO }) {
        const query = `INSERT INTO RECIPE
                    (USRID, TITLE, COOKING_TIME, CALORIES, SERVINGS, CUISINEID, COURSEID, PHOTO_ID) VALUES
                    (${USRID}, '${TITLE}', ${COOKING_TIME || 'NULL'}, ${CALORIES || 'NULL'}, ${SERVINGS || 'NULL'}, ${CUISINEID}, ${COURSEID}, '${`${PHOTO}` || 'NULL'}')
                    SELECT SCOPE_IDENTITY() AS id`;
        return SQL.sendQuery(query);
    }

    static addInstructions(RECIPEID, INSTRUCTIONS) {
        if (!INSTRUCTIONS.length) {
            return Promise.resolve();
        }
        const query = 'INSERT INTO INSTRUCTION(RECIPEID, CONTENT, ORDER_NUM) VALUES '.concat(
            INSTRUCTIONS.reduce(
                (innerQuery, instruction, index) => innerQuery.concat(`(${RECIPEID}, '${instruction}', ${index})${index < INSTRUCTIONS.length - 1 ? ',' : ''}`),
                ''
            )
        );
        return SQL.sendQuery(query);
    }

    static addIngredients(RECIPEID, INGREDIENTS) {
        if (!INGREDIENTS.length) {
            return Promise.resolve();
        }
        const query = 'INSERT INTO INGREDIENT(RECIPEID, INAME, UNIT, AMOUNT) VALUES '.concat(
            INGREDIENTS.reduce(
                (innerQuery, { amount, unit, name }, index) => innerQuery.concat(`(${RECIPEID}, '${name}', '${unit}', ${amount || 'NULL'})${index < INGREDIENTS.length - 1 ? ',' : ''}`),
                ''
            )
        );
        return SQL.sendQuery(query);
    }

    static fetchRecipesForCategory(CATEGORYID, CATEGORYTYPE) {
        const query = `SELECT RECIPEID, PHOTO_ID, TITLE, COOKING_TIME, CALORIES, SERVINGS FROM RECIPE WHERE ${CATEGORYTYPE}ID = ${CATEGORYID}`;
        return SQL.sendQuery(query);
    }

    static fetchRecipe(RECIPEID) {
        const query = `SELECT COOKING_TIME, CALORIES, SERVINGS, CUISINEID, COURSEID, PHOTO_ID FROM RECIPE WHERE RECIPEID=${RECIPEID}`;
        return SQL.sendQuery(query);
    }

    static fetchInstructions(RECIPEID) {
        const query = `SELECT CONTENT, ORDER_NUM FROM INSTRUCTION WHERE RECIPEID=${RECIPEID}`;
        return SQL.sendQuery(query);
    }

    static fetchIngredients(RECIPEID) {
        const query = `SELECT INAME, AMOUNT, UNIT FROM INGREDIENT WHERE RECIPEID=${RECIPEID}`;
        return SQL.sendQuery(query);
    }

    static fetchCategoryName(CATEGORYID, CATEGORYTYPE) {
        const query = `SELECT CNAME FROM ${CATEGORYTYPE} WHERE ${CATEGORYTYPE}ID=${CATEGORYID}`;
        return SQL.sendQuery(query);
    }

    static addFavourite(USERID, RECIPEID) {
        const query = `INSERT INTO FAVOURITE(USRID, RECIPEID) VALUES(${USERID}, ${RECIPEID})`;
        return SQL.sendQuery(query);
    }

    static deleteFavourite(USERID, RECIPEID) {
        const query = `DELETE FROM FAVOURITE WHERE USRID=${USERID} AND RECIPEID=${RECIPEID}`;
        return SQL.sendQuery(query);
    }

    static getFavourites(USERID) {
        const query = `SELECT RECIPEID FROM FAVOURITE WHERE USRID=${USERID}`;
        return SQL.sendQuery(query);
    }

    static getFavouriteRecipe(RECIPEID) {
        const query = `SELECT RECIPEID, PHOTO_ID, TITLE, COOKING_TIME, CALORIES, SERVINGS FROM RECIPE WHERE RECIPEID=${RECIPEID}`;
        return SQL.sendQuery(query);
    }

    static fetchTop10Recipes() {
        const query = `SELECT TOP 10 TITLE, PHOTO_ID, RECIPEID FROM RECIPE ORDER BY RECIPEID DESC`;
        return SQL.sendQuery(query);
    }

    static isFavourite(USRID, RECIPEID) {
        const query = `SELECT COUNT(*) FROM FAVOURITE WHERE USRID=${USRID} AND RECIPEID=${RECIPEID}`;
        return SQL.sendQuery(query);
    }
}

module.exports = SQL;