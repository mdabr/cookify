const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    try {
        const recipes = await Mongo.fetchTopRecipes(10);

        return res.status(200).send(recipes.map(recipe => ({
            id: recipe._id,
            image: recipe.photoId,
            title: recipe.title
        })));
    } catch (error) {
        logger.error(`Error fetching recipes: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;