const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    try {
        const { userid } = req.query;
        const favourites = await Mongo.getFavourites(userid);
        const recipesIds = favourites.map(favourite => favourite.recipeId);
        const favouriteRecipes = await Promise.all(recipesIds.map(recipeId => Mongo.getRecipe(recipeId)));

        res.status(200).send(favouriteRecipes);
    } catch (error) {
        logger.error(`Error fetching favourite recipes: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;