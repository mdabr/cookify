const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    const RECIPEID = req.query.id;

    try {
        if (RECIPEID === 'undefined') {
            throw new Error('No recipe ID passed');
        }

        const recipe = await Mongo.fetchRecipe(RECIPEID);

        logger.info('Recipe successfully fetched!');
        return res.status(200).send({ recipe: {
            ...recipe,
            course: await Mongo.fetchCourseName(recipe.courseId),
            cuisine: await Mongo.fetchCuisineName(recipe.cuisineId)
        } });
    } catch (error) {
        logger.error(`Error fetching recipe: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;