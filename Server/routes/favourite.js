const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    const { userid, recipeid, checked } = req.query;

    try {
        await Mongo.editFavourite(userid, recipeid, checked);
        logger.info(`RECIPE: ${recipeid} successfully ${checked === 'true' ? 'saved to' : 'deleted from'} favourites`);
        return res.status(200).end();
    } catch (error) {
        logger.error(`Error editing favourite recipe: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;