const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    try {
        const CATEGORYID = req.query.id;
        const CATEGORYTYPE = req.query.type;
    
        if (CATEGORYID === 'undefined' || CATEGORYTYPE === 'undefined') {
            throw new Error('Missing category id or category type');
        }

        const recipes = await Mongo.fetchRecipesForCategory(CATEGORYID, CATEGORYTYPE);

        res.status(200).send(recipes);
    } catch (error) {
        logger.error(`Error fetching recipes: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;