const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    try {
        const cuisines = await Mongo.fetchCuisines();

        logger.info(`Fetched cuisines: ${cuisines.map(course => course.name).join(', ')}`);

        res.status(200).send(cuisines);
    } catch (error) {
        logger.error(`Error fetching cuisines: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;