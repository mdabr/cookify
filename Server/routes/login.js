const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.post('/', async (req, res) => {
    const { EMAIL, PASSWORD } = req.body;

    try {
        const user = await Mongo.verifyLogin(EMAIL, PASSWORD);

        if (!user) {
            throw {
                number: 2666
            };
        }

        logger.info(`USER: { EMAIL: '${EMAIL}' } is registered with provided password!`);
        return res.status(200).send(user);
    } catch (error) {
        logger.error(`Error verifying user: ${error.number}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;