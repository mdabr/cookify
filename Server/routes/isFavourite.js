const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    const { userid, recipeid } = req.query;

    try {
        const isFavourite = await Mongo.isFavourite(userid, recipeid);
        return res.status(200).send({ isFavourite });
    } catch (error) {
        logger.error(`Error reading favourite recipe: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;