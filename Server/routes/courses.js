const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    try {
        const courses = await Mongo.fetchCourses();

        logger.info(`Fetched courses: ${courses.map(course => course.name).join(', ')}`);

        res.status(200).send(courses);
    } catch (error) {
        logger.error(`Error fetching courses: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;