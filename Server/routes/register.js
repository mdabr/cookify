const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.post('/', async (req, res) => {
    const { NAME, EMAIL, PASSWORD } = req.body;

    try {
        if (!(await Mongo.isEmailUnique(EMAIL))) {
            throw new Error('This email address is taken');
        }

        await Mongo.registerUser(NAME, EMAIL, PASSWORD);
        logger.info(`USER: { NAME: '${NAME}', EMAIL: '${EMAIL}' } successfully registered`);
        return res.status(200).send(`${EMAIL} successfully registered!`);
    } catch (error) {
        logger.error(error.message);
        return res.status(500).send({ error });
    }
});

module.exports = router;