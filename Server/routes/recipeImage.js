const express = require('express');
const multer = require('multer');
const fs = require('fs-extra');

const { IMAGES } = require('../utils/Constants').URI;
const logger = require('../utils/Logger');

const upload = multer({
    dest: './temp'
});
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const files = await fs.readdir(IMAGES);
        res.status(200).send(
            files.find(file => file === req.query.id) || {}
        );
    } catch (error) {
        res.status(500).send({ error });
    }
});

router.post('/', upload.single('file'), async (req, res) => {
    try {
        const tempPath = req.file.path;
        const ext = req.file.originalname.split('.').pop();
        const targetPath = `${IMAGES}/${new Date().getTime()}.${ext}`;

        await fs.rename(tempPath, targetPath);
        logger.warn(`Image posted: ${req.file.originalname} under name: ${targetPath.split('/').pop()}`);
        res.status(200)
            .contentType('text/plain')
            .end(JSON.stringify({ id: targetPath.split('/').pop() }));
    } catch (error) {
        res.status(500).send({ error });
    }
});

module.exports = router;