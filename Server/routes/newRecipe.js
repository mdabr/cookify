const express = require('express');
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');
const router = express.Router();

router.post('/', async (req, res) => {
    // TODO: send cuisineId instead of name, send courseId instead of name
    const { USRID, NAME, TIME, SERVINGS, CALORIES, COURSE, IMAGE, CUISINE, INGREDIENTS, INSTRUCTIONS } = req.body;

    try {
        const newRecipe = {
            title: NAME,
            cookingTime: TIME,
            calories: CALORIES,
            servings: SERVINGS,
            cuisineId: await Mongo.getCuisineId(CUISINE),
            courseId: await Mongo.getCourseId(COURSE),
            creatorId: USRID,
            photoId: IMAGE,
            instructions: INSTRUCTIONS.map((instruction, index) => ({ content: instruction, orderNum: index + 1 })),
            ingredients: INGREDIENTS.map(({ amount, unit, name }) => ({ name, unit, amount }))
        };
        const response = await Mongo.addNewRecipe(newRecipe);
        
        logger.info(`New recipe added: ${newRecipe}`);
        return res.status(200).send({ RECIPEID: response.insertedId });
    } catch (error) {
        logger.error(`Failed to add new recipe: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;