const register = require('./register');
const login = require('./login');
const cuisines = require('./cuisines');
const courses = require('./courses');
const recipeImage = require('./recipeImage');
const newRecipe = require('./newRecipe');
const categories = require('./categories');
const recipes = require('./recipes');
const recipe = require('./recipe');
const favourite = require('./favourite');
const favourites = require('./favourites');
const slider = require('./slider');
const isFavourite = require('./isFavourite');

module.exports = {
    register,
    login,
    cuisines,
    courses,
    recipeImage,
    newRecipe,
    categories,
    recipes,
    recipe,
    favourite,
    favourites,
    slider,
    isFavourite
};
