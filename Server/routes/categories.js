const express = require('express');
const router = express.Router();
const Mongo = require('../mongo/Mongo');
const logger = require('../utils/Logger');

router.get('/', async (req, res) => {
    try {
        const [courses, cuisines] = await Promise.all([Mongo.fetchCourses(), Mongo.fetchCuisines()]);
        const coursesWithType = courses.map(course => ({ ...course, type: 'COURSE' }));
        const cuisinesWithType = cuisines.map(cuisine => ({ ...cuisine, type: 'CUISINE' }));
        const categories = coursesWithType.concat(cuisinesWithType);

        logger.info(`Fetched categories: ${categories.map(category => category.name).join(', ')}`);

        res.status(200).send({
            categories: categories.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0)
        });
    } catch (error) {
        logger.error(`Error fetching categories: ${error.message}`);
        return res.status(500).send({ error });
    }
});

module.exports = router;