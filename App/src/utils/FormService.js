import { ENDPOINTS } from 'utils/constants';

const { REGISTER, LOGIN, NEW_RECIPE } = ENDPOINTS;

const postForm = endpoint => form => fetch(endpoint, {
    body: JSON.stringify(form),
    headers: {
        'Content-Type': 'application/json'
    },
    method: 'POST'
});

const submitRegistration = postForm(REGISTER);
const submitLogin = postForm(LOGIN);
const submitRecipe = postForm(NEW_RECIPE);

export {
    submitRegistration,
    submitLogin,
    submitRecipe
};
