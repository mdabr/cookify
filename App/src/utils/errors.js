const ERRORS = {
    500: 'Server error!',
    404: 'Resource not found!',
    2627: 'Email already registered!',
    2666: 'Email or password are incorrect!',
    DEFAULT: 'Something went wrong!'
};
const getErrorMessage = code => {
    window.console.error(code);
    return ERRORS[code] || ERRORS.DEFAULT;
};
const isError = code => ERRORS[code] !== undefined;

export {
    isError,
    getErrorMessage
};
