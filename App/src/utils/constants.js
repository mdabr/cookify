const SERVER_PORT = 1337;
const ADDRESS = 'localhost'; /* or localhost */
const BASE_URL = `http://${ADDRESS}:${SERVER_PORT}`;
const ENDPOINTS = {
    IMAGES: {
        BACKGROUND: '/background.jpg',
        PRELOGIN: '/prelogin.jpg',
        NEW_RECIPE: '/new-recipe.svg',
        CATEGORIES: '/categories.svg',
        BLOG: '/blog.svg',
        SAVED_RECIPES: '/saved-recipes.svg',
        CLOSE: '/close.svg',
        REMOVE: '/remove.svg',
        RETURN: '/return.svg',
        CATEGORY: '/categories/:name.jpg',
        NOTHING: '/nothing.svg',
        RECIPE: '/images/:id'
    },
    REGISTER: '/register',
    LOGIN: '/login',
    CUISINES: '/cuisines',
    COURSES: '/courses',
    UPLOAD_RECIPE_IMAGE: '/recipe-image',
    GET_RECIPE_IMAGE: '/recipe-image?id=:id',
    NEW_RECIPE: '/new-recipe',
    CATEGORIES: '/categories',
    RECIPES: '/recipes?id=:id&type=:type',
    RECIPE: '/recipe?id=:id',
    FAVOURITE: '/favourite?userid=:userid&recipeid=:recipeid&checked=:checked',
    IS_FAVOURITE: '/is-favourite?userid=:userid&recipeid=:recipeid',
    FAVOURITES: '/favourites?userid=:userid',
    SLIDER: '/slider'
};
const ROUTES = {
    MAIN_PAGE: {
        BASE: '/main-page',
        TITLE_PAGE: '/main-page/title-page',
        BLOG: '/main-page/blog',
        NEW_RECIPE: '/main-page/new-recipe',
        SAVED_RECIPES: '/main-page/saved-recipes',
        CATEGORIES: '/main-page/categories',
        CATEGORY: '/main-page/category/:id',
        BLOG_POST: '/main-page/blog-post/:id',
        RECIPE: '/main-page/recipe/:id',
        NEW_BLOG_POST: '/main-page/new-blog-post'
    },
    PRELOGIN: {
        LOGIN: '/prelogin',
        REGISTER: '/prelogin/register'
    }
};
const TITLE = 'Cookify';
const SUBTITLE = 'From people, for people';

const ENDPOINTS_PROXY_HANDLER = {
    get: (obj, prop) => {
        if (typeof obj[prop] === 'object' && obj[prop] !== null) {
            return new Proxy(obj[prop], ENDPOINTS_PROXY_HANDLER);
        }
        return `${BASE_URL}${obj[prop]}`;
    }
};
const ENDPOINTS_PROXY = new Proxy(ENDPOINTS, ENDPOINTS_PROXY_HANDLER);

export {
    ENDPOINTS_PROXY as ENDPOINTS,
    TITLE,
    SUBTITLE,
    ROUTES
};
