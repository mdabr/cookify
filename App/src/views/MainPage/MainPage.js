import React from 'react';
import './MainPage.css';
import { BottomBar, Profile } from 'components';
import { ENDPOINTS, TITLE, ROUTES } from 'utils/constants';
import { Route, Redirect } from 'react-router-dom';
import { TitleRoute, NewRecipeRoute, BlogRoute, NewBlogPostRoute, BlogPostRoute, CategoriesRoute, CategoryRoute, RecipeRoute, SavedRecipesRoute } from 'views/MainPage/routes';

const { TITLE_PAGE, BLOG, NEW_RECIPE, SAVED_RECIPES, CATEGORIES, BLOG_POST, RECIPE, NEW_BLOG_POST, CATEGORY } = ROUTES.MAIN_PAGE;
const { BACKGROUND } = ENDPOINTS.IMAGES;

const MainPage = () => <div className="main-page-container">
    <div className="main-page-background" style={{ backgroundImage: `url(${BACKGROUND})` }}>
        <div className="main-page-backdrop">
            <Route path={TITLE_PAGE} component={() => <TitleRoute title={TITLE} />} />
            <Route path={BLOG} component={() => <BlogRoute />} />
            <Route path={NEW_RECIPE} component={() => <NewRecipeRoute />} />
            <Route path={SAVED_RECIPES} component={() => <SavedRecipesRoute />} />
            <Route path={CATEGORIES} component={() => <CategoriesRoute />} />
            <Route path={BLOG_POST} component={router => router.location.blogObj
                ? <BlogPostRoute blogObj={router.location.blogObj} />
                : <Redirect to={TITLE_PAGE} />} />
            <Route path={RECIPE} component={router => router.location.recipeObj
                ? <RecipeRoute recipeObj={router.location.recipeObj} />
                : <Redirect to={CATEGORIES} />} />
            <Route path={NEW_BLOG_POST} component={() => <NewBlogPostRoute />} />
            <Route path={CATEGORY} component={router => router.location.categoryObj
                ? <CategoryRoute categoryObj={router.location.categoryObj} />
                : <Redirect to={CATEGORIES} />} />
        </div>
    </div>
    <Profile />
    <BottomBar />
</div>;

export default MainPage;
