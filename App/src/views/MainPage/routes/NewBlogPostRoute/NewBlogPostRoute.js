import React from 'react';
import './NewBlogPostRoute.css';
import { ENDPOINTS, ROUTES } from 'utils/constants';
import { Close, NewBlogPostForm } from 'components';
import styled from 'styled-components';

const { BLOG } = ROUTES.MAIN_PAGE;
const { RETURN } = ENDPOINTS.IMAGES;
const Title = styled.h3`
    color: white;
`;

const NewBlogPostRoute = () => <div className="new-blog-post-container">
    <Title>New blog post</Title>
    <NewBlogPostForm />
    <Close link={BLOG} image={RETURN} size={5} />
</div>;

export default NewBlogPostRoute;