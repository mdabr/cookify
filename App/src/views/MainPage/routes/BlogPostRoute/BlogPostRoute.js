import React from 'react';
import { ROUTES, ENDPOINTS } from 'utils/constants';
import styled from 'styled-components';
import { Close, BlogPost } from 'components';
import './BlogPostRoute.css';

const { TITLE_PAGE } = ROUTES.MAIN_PAGE;

const Title = styled.h3`
    color: white;
`;

const { RETURN, CLOSE } = ENDPOINTS.IMAGES;

const BlogPostRoute = ({ blogObj }) => {
    const { id, title, image, backRoute } = blogObj;
    return <div className="blog-post-container">
        <Title>{title}</Title>
        <BlogPost blogObj={blogObj} id={id} image={image} />
        <Close link={backRoute || TITLE_PAGE} image={backRoute ? RETURN : CLOSE} size={5} />
    </div>;
};

export default BlogPostRoute;