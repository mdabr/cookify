import React, { useState, useEffect } from 'react';
import './RecipeRoute.css';
import { Close, Recipe } from 'components';
import { ENDPOINTS } from 'utils/constants';
import styled from 'styled-components';
import { connect } from 'react-redux';

const { RETURN } = ENDPOINTS.IMAGES;
const { FAVOURITE, IS_FAVOURITE } = ENDPOINTS;

const Title = styled.h3`
    color: white;
    margin: 0;
`;
const SaveRecipe = styled.label`
    position: absolute;
    top: 1vw;
    right: 2vw;
    color: white;
    width: 10vw;
`;
const Input = styled.input`
    background-color: transparent;
    border: 1px dotted #000;
    opacity: 0;
`;
const Checkmark = styled.span`
    position: absolute;
    top: 0;
    left: 0;
    height: 2vw;
    width: 2vw;
    background-color: #eee;
    border-radius: 10px;
`;
const Label = styled.span`
    position: absolute;
    top: 0;
    left: 2vw;
    width: 8vw;
`;

const RecipeRoute = ({ recipeObj, user }) => {
    const { title, id } = recipeObj;
    const [isSaved, setIsSaved] = useState(false);
    const toggleSaved = recipeId => event => fetch(
        FAVOURITE
            .replace(':userid', user.id)
            .replace(':recipeid', recipeId)
            .replace(':checked', event.target.checked)
    );
    useEffect(() => {
        fetch(IS_FAVOURITE.replace(':userid', user.id).replace(':recipeid', id))
            .then(response => response.json())
            .then(saved => setIsSaved(saved.isFavourite));
    }, []);
    return <div className="recipe-route-container">
        <Title>{title}</Title>
        <Recipe id={id} />
        <SaveRecipe htmlFor="saved-recipe">
            <Label>Save for later</Label>
            <Input id="saved-recipe" type="checkbox" onChange={event => setIsSaved(event.target.checked)} onClick={toggleSaved(id)} checked={isSaved}/>
            <Checkmark className="checkmark" ></Checkmark>
        </SaveRecipe>
        <Close image={RETURN} link="/main-page/categories" size={5} />
    </div>;
};

export default connect(
    ({ user }) => ({ user })
)(RecipeRoute);