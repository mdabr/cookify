import React from 'react';
import './CategoryRoute.css';
import styled from 'styled-components';
import { Close, RecipeList } from 'components';
import { ROUTES, ENDPOINTS } from 'utils/constants';

const Title = styled.h3`
    color: white;
`;
const { CATEGORIES } = ROUTES.MAIN_PAGE;
const { RETURN } = ENDPOINTS.IMAGES;

const CategoryRoute = ({ categoryObj }) => {
    const { id, name, type } = categoryObj;
    return <div className="category-container">
        <Title>{name}</Title>
        <RecipeList categoryId={id} categoryType={type} />
        <Close link={CATEGORIES} image={RETURN} size={5} />
    </div>;
};

export default CategoryRoute;