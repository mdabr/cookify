import React, { Suspense } from 'react';
import { FavouriteList, Close } from 'components';
import { ROUTES, ENDPOINTS } from 'utils/constants';
import styled from 'styled-components';
import './SavedRecipesRoute.css';
import Loader from 'react-loader-spinner';

const { CLOSE } = ENDPOINTS.IMAGES;
const { TITLE_PAGE } = ROUTES.MAIN_PAGE;

const Title = styled.h3`
    color: white;
`;

const SavedRecipesRoute = () => <div className="saved-recipes-container">
    <Title>Saved recipes</Title>
    <Suspense fallback={<Loader type="ThreeDots" color="#fff" height="300" width="300" />}>
        <FavouriteList />
    </Suspense>
    <Close image={CLOSE} link={TITLE_PAGE} size={5} />
</div>;

export default SavedRecipesRoute;