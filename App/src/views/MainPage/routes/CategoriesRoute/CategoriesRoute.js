import React from 'react';
import { ROUTES, ENDPOINTS } from 'utils/constants';
import styled from 'styled-components';
import { Close, CategoriesList } from 'components';
import './CategoriesRoute.css';

const { TITLE_PAGE } = ROUTES.MAIN_PAGE;
const { CLOSE } = ENDPOINTS.IMAGES;

const Title = styled.h3`
    color: white;
`;

const CategoriesRoute = () => <div className="categories-container">
    <Title>Categories</Title>
    <CategoriesList />
    <Close link={TITLE_PAGE} image={CLOSE} size={5} />
</div>;

export default CategoriesRoute;