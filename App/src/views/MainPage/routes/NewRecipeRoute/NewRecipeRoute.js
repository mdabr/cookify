import React from 'react';
import './NewRecipeRoute.css';
import { ROUTES, ENDPOINTS } from 'utils/constants';
import { Close, NewRecipeForm } from 'components';
import styled from 'styled-components';

const { TITLE_PAGE } = ROUTES.MAIN_PAGE;

const Title = styled.h3`
    color: white;
`;

const NewRecipeRoute = () => <div className="new-recipe-container">
    <Close link={TITLE_PAGE} image={ENDPOINTS.IMAGES.CLOSE} size={5} />
    <Title>New recipe</Title>
    <NewRecipeForm />
</div>;

export default NewRecipeRoute;