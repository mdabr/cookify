import React from 'react';
import './BlogRoute.css';
import { ENDPOINTS, ROUTES } from 'utils/constants';
import { Close, BlogPostList } from 'components';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const { TITLE_PAGE, NEW_BLOG_POST } = ROUTES.MAIN_PAGE;
const { CLOSE } = ENDPOINTS.IMAGES;

const Title = styled.h3`
    color: white;
`;

const BlogRoute = () => <div className="blog-container">
    <Title>Blog</Title>
    <BlogPostList></BlogPostList>
    <Close link={TITLE_PAGE} image={CLOSE} size={5} />
    <Link to={NEW_BLOG_POST}>
        <input type="button" className="new-blog-post-button" value="Add new post" />
    </Link>
</div>;

export default BlogRoute;