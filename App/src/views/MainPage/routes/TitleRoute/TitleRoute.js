import React from 'react';
import './TitleRoute.css';

const TitleRoute = ({ title }) => <p className="title">{title}</p>;

export default TitleRoute;