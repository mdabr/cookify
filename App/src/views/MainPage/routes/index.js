import TitleRoute from './TitleRoute/TitleRoute';
import NewRecipeRoute from './NewRecipeRoute/NewRecipeRoute';
import BlogRoute from './BlogRoute/BlogRoute';
import NewBlogPostRoute from './NewBlogPostRoute/NewBlogPostRoute';
import BlogPostRoute from './BlogPostRoute/BlogPostRoute';
import CategoriesRoute from './CategoriesRoute/CategoriesRoute';
import CategoryRoute from './CategoryRoute/CategoryRoute';
import RecipeRoute from './RecipeRoute/RecipeRoute';
import SavedRecipesRoute from './SavedRecipesRoute/SavedRecipesRoute';

export {
    TitleRoute,
    NewRecipeRoute,
    BlogRoute,
    NewBlogPostRoute,
    BlogPostRoute,
    CategoriesRoute,
    CategoryRoute,
    RecipeRoute,
    SavedRecipesRoute
};
