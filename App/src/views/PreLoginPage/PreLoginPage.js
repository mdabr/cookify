import React from 'react';
import styled from 'styled-components';
import './PreLoginPage.css';
import { TITLE, SUBTITLE, ROUTES, ENDPOINTS } from 'utils/constants';
import { LoginForm, RegisterForm } from 'components';
import { Route } from 'react-router-dom';

const { LOGIN, REGISTER } = ROUTES.PRELOGIN;

const PreLoginContainer = styled.div`
    width: 100vw;
    height: 100vh;
    display: grid;
    grid-template-columns: 2fr 3fr 2fr;
    grid-template-rows: 1fr;
    background-image: url(${props => props.background});
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
`;

const PreLoginPage = () => <PreLoginContainer background={ENDPOINTS.IMAGES.PRELOGIN}>
    <div className="middle-column">
        <h2 className="title">{TITLE}</h2>
        <h3 className="subtitle">{SUBTITLE}</h3>
        <Route exact path={LOGIN} component={() => <LoginForm />} />
        <Route path={REGISTER} component={() => <RegisterForm />} />
    </div>
</PreLoginContainer>;

export default PreLoginPage;
