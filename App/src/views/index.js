import MainPage from './MainPage/MainPage';
import PreLoginPage from './PreLoginPage/PreLoginPage';
import NotFound from './NotFound/NotFound';

export {
    PreLoginPage,
    MainPage,
    NotFound
};
