import React from 'react';
import './NotFound.css';
import { Link } from 'react-router-dom';

const NotFound = () => <div className="not-found-container">
    <h1>404</h1>
    <h3>Oops! Looks like you're lost! <Link to="/">Go back!</Link></h3>
</div>;

export default NotFound;