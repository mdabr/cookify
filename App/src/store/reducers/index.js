import alert from './alert';
import user from './user';
import { combineReducers } from 'redux';

export default combineReducers({
    alert,
    user
});