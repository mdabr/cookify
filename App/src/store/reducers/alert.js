const defaultAlert = {
    level: null,
    text: null
};

const alert = (state = defaultAlert, { type, text, level }) => {
    switch (type) {
    case 'CHANGE_ALERT':
        return {
            level,
            text
        };
    case 'RESET_ALERT':
        return defaultAlert;
    default:
        return state;
    }
};

export default alert;