const defaultUser = {
    id: null,
    name: null,
    isEditor: false,
    isVerified: false
};

const user = (state = defaultUser, { type, id, name, isEditor, isVerified }) => {
    switch (type) {
    case 'CHANGE_USER':
        return {
            id,
            name,
            isEditor,
            isVerified
        };
    case 'RESET_USER':
        return defaultUser;
    default:
        return state;
    }
};

export default user;