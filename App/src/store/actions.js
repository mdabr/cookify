export const changeAlert = ({ level, text }) => ({
    type: 'CHANGE_ALERT',
    level,
    text
});

export const resetAlert = () => ({
    type: 'RESET_ALERT',
    level: '',
    text: ''
})

export const changeUser = ({ id, name, isEditor, isVerified }) => ({
    type: 'CHANGE_USER',
    id,
    name,
    isEditor,
    isVerified
});

export const resetUser = () => ({
    type: 'RESET_USER',
    id: null,
    name: null,
    isEditor: false,
    isVerified: false
});