import React from 'react';
import './BlogPostList.css';
import BlogPostItem from './BlogPostItem/BlogPostItem';
import { ROUTES } from 'utils/constants';

// Issue #48: Fetch post list from backend
const postList = [
    {
        id: 1,
        title: 'Lorem ipsum',
        image: '',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in finibus odio. Aliquam viverra tempor euismod. Vestibulum a metus venenatis, sollicitudin mi sed, sagittis massa. Nam molestie dolor nec odio placerat viverra. Phasellus sit amet tempor mi. Etiam ullamcorper hendrerit nunc eget blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet volutpat massa, in efficitur dolor feugiat eu.'
    },
    {
        id: 2,
        title: 'Lorem ipsum',
        image: '',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in finibus odio. Aliquam viverra tempor euismod. Vestibulum a metus venenatis, sollicitudin mi sed, sagittis massa. Nam molestie dolor nec odio placerat viverra. Phasellus sit amet tempor mi. Etiam ullamcorper hendrerit nunc eget blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet volutpat massa, in efficitur dolor feugiat eu.'
    },
    {
        id: 3,
        title: 'Lorem ipsum',
        image: '',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in finibus odio. Aliquam viverra tempor euismod. Vestibulum a metus venenatis, sollicitudin mi sed, sagittis massa. Nam molestie dolor nec odio placerat viverra. Phasellus sit amet tempor mi. Etiam ullamcorper hendrerit nunc eget blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet volutpat massa, in efficitur dolor feugiat eu.'
    },
    {
        id: 4,
        title: 'Lorem ipsum',
        image: '',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in finibus odio. Aliquam viverra tempor euismod. Vestibulum a metus venenatis, sollicitudin mi sed, sagittis massa. Nam molestie dolor nec odio placerat viverra. Phasellus sit amet tempor mi. Etiam ullamcorper hendrerit nunc eget blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet volutpat massa, in efficitur dolor feugiat eu.'
    }
];

const { BLOG } = ROUTES.MAIN_PAGE;

const BlogPostList = () => <div className="blog-post-list-container">
    {postList.map((post, index) => <BlogPostItem backRoute={BLOG} key={index} {...post} />)}
</div>;

export default BlogPostList;