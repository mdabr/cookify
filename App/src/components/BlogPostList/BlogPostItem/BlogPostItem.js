import React from 'react';
import './BlogPostItem.css';
import styled from 'styled-components';
import { ROUTES } from 'utils/constants';
import { Link } from 'react-router-dom';

const PostImage = styled.div`
    background-image: url(${props => props.image});
    background-color: #666;
    width: 23%;
    height: 100%;
    float: left;
    margin-right: 2%;
`;

const PostText = styled.div`
    width: 75%;
    height: 100%;
    float: left;
`;

const Title = styled.h4`
    color: white;
    margin: 0;
    text-align: left;
`;

const EllipsifiedDescription = styled.div`
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
`;

const { BLOG_POST, BLOG } = ROUTES.MAIN_PAGE;
const blogIdPlaceHolder = ':id';
const routeToLink = (route, placeholder) => id => route.replace(placeholder, id);
const blogRouteToLink = routeToLink(BLOG_POST, blogIdPlaceHolder);

const BlogPostItem = ({ id, title, image, description }) => {
    const blogObj = {
        id,
        title,
        image,
        backRoute: BLOG,
        description
    };
    return <Link to={{
        pathname: blogRouteToLink(id),
        blogObj
    }}>
        <div className="blog-post-item-container">
            <PostImage image={image} />
            <PostText>
                <Title>{title}</Title>
                <EllipsifiedDescription>{description}</EllipsifiedDescription>
            </PostText>
        </div>
    </Link>;
};

export default BlogPostItem;