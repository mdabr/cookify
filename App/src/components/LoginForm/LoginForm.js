import React from 'react';
import './LoginForm.css';
import { Link, withRouter } from 'react-router-dom';
import { ROUTES } from 'utils/constants';
import { submitLogin } from 'utils/FormService';
import { getErrorMessage } from 'utils/errors';
import { connect } from 'react-redux';
import { changeAlert, changeUser } from 'store/actions';

const { REGISTER } = ROUTES.PRELOGIN;
const { TITLE_PAGE } = ROUTES.MAIN_PAGE;

const LoginForm = withRouter(({ history, dispatch }) => {
    const onSubmit = event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const data = {};
        for (const key of formData.keys()) {
            data[key] = formData.get(key);
        }
        submitLogin(data)
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    throw response.error;
                }

                const { _id, name, isEditor, isVerified } = response;
                const user = {
                    id: _id,
                    name,
                    isEditor,
                    isVerified
                };

                console.log(user);

                dispatch(changeUser(user));
                window.sessionStorage.setItem('user', JSON.stringify(user));
                history.push(TITLE_PAGE);
            })
            .catch(error => dispatch(changeAlert({ text: getErrorMessage(error.number), level: 'error' })));
    };

    return <form className="login-form" autoComplete="off" onSubmit={onSubmit} action={TITLE_PAGE}>
        <label htmlFor="EMAIL">
            <span>Email</span>
            <input id="EMAIL" name="EMAIL" type="email" required />
        </label>
        <label htmlFor="PASSWORD">
            <span>Password</span>
            <input id="PASSWORD" name="PASSWORD" type="password" required />
        </label>
        <input type="submit" value="Login" />
        <span className="no-account-yet">Don't have an account yet?</span>
        <Link className="register-button" to={REGISTER}>Register</Link>
    </form>;
});

export default connect()(LoginForm);