import React from 'react';
import styled from 'styled-components';
import { ENDPOINTS } from 'utils/constants';

const { NOTHING } = ENDPOINTS.IMAGES;

const Nothing = styled.div`
    width: ${props => props.width}%;
    height: ${props => props.height}%;
    color: white;
    padding-top: 5%;
    display: flex;
    flex-direction: column;
    align-items: center;
`;
const NothingText = styled.p`
    margin-bottom: 5%;
`;
const NothingImage = styled.img`
    object-fit: contain;
    width: 50%;
    height: 50%;
`;

const NothingHereYet = ({ width = 100, height = 100 }) => <Nothing width={width} height={height}>
    <NothingText>There's nothing here yet!</NothingText>
    <NothingImage src={NOTHING} />
</Nothing>;

export default NothingHereYet;