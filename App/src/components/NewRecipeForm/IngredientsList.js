import React, { useState, useRef } from 'react';
import { ENDPOINTS } from 'utils/constants';
import styled from 'styled-components';
import { v4 } from 'uuid';

const { REMOVE } = ENDPOINTS.IMAGES;
const RemoveItem = styled.div`
    background-image: url(${props => props.image});
    width: ${props => props.size}vw;
    height: ${props => props.size}vw;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    float: left;

    &:hover {
        cursor: pointer;
    }
`;

const Ingredient = ({ amount, unit, name, removeIngredient }) => <div className="ingredient-container">
    <RemoveItem className="remove-ingredient" onClick={removeIngredient} image={REMOVE} size={1} />
    <span className="ingredient-text">{amount} {unit} {name}</span>
</div>;

const generateIngredients = (ingredients, removeIngredient) => ingredients.map(
    (ingredient, index) => <Ingredient
        key={v4()}
        {...ingredient}
        removeIngredient={removeIngredient(index)}
    />
);

const IngredientsList = ({ changeIngredients }) => {
    const [ingredients, setIngredients] = useState([]);
    const amountInput = useRef(null);
    const unitInput = useRef(null);
    const nameInput = useRef(null);

    const addIngredient = () => {
        const ingredient = {
            amount: amountInput.current.value,
            unit: unitInput.current.value,
            name: nameInput.current.value
        };
        const iSIngredientComplete = ingredient.name !== '';
        const newIngredients = [...ingredients].concat(iSIngredientComplete ? ingredient : []);
        clearInputs(iSIngredientComplete);
        changeIngredients(newIngredients);
        setIngredients(newIngredients);
    };

    function clearInputs(shouldClear) {
        amountInput.current.value = shouldClear ? '' : amountInput.current.value;
        unitInput.current.value = shouldClear ? '' : unitInput.current.value;
        nameInput.current.value = shouldClear ? '' : nameInput.current.value;
    }

    const onKeyDown = event => {
        if (event.keyCode === 13) {
            event.preventDefault();
            addIngredient();
        }
    };

    const removeIngredient = index => () => {
        ingredients.splice(index, 1);
        changeIngredients([...ingredients]);
        setIngredients([...ingredients]);
    };
    return <div className="ingredients">
        <label>Ingredients</label>
        {generateIngredients(ingredients, removeIngredient)}
        <div className="add-ingredient">
            <input onKeyDown={onKeyDown} ref={amountInput} className="amount" type="number" placeholder="Amount" />
            <input onKeyDown={onKeyDown} ref={unitInput} className="unit" type="text" placeholder="Unit" />
            <input onKeyDown={onKeyDown} ref={nameInput} className="name" type="text" placeholder="Name" required={ingredients.length === 0} />
            <input onClick={addIngredient} type="button" value="Add" />
        </div>
    </div>;
};

export default IngredientsList;