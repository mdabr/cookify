import React, { useState, useEffect } from 'react';
import { v4 } from 'uuid';

const OptionItem = ({ name }) => <option value={name}>{name}</option>;

const OptionsList = ({ url }) => {
    const [options, setOptions] = useState([]);

    useEffect(() => {
        fetch(url)
            .then(response => response.json())
            .then(data => setOptions(data));
    }, []);

    return options.map(({ name }) => <OptionItem key={v4()} name={name} />);
};

export default OptionsList;