import React, { Suspense, lazy, useState } from 'react';
import './NewRecipeForm.css';
import Dropzone from 'react-dropzone';
import { ENDPOINTS, ROUTES } from 'utils/constants';
import InstructionsList from './InstructionsList';
import IngredientsList from './IngredientsList';
import { submitRecipe } from 'utils/FormService';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { changeAlert } from 'store/actions';

const { RECIPE } = ROUTES.MAIN_PAGE;
const { COURSES, CUISINES, UPLOAD_RECIPE_IMAGE } = ENDPOINTS;
const OptionsList = lazy(() => import('./OptionsList'));

// Issue #42: Add onBlur behaviour
// Issue #43: Split form into components
const NewRecipeForm = ({ user, history, dispatch }) => {
    const [image, setImage] = useState(null);
    const [ingredients, setIngredients] = useState([]);
    const [instructions, setInstructions] = useState([]);

    function uploadRecipeImage() {
        if (!image) {
            return Promise.resolve({ id: null });
        }

        const imageData = new FormData();
        imageData.append('file', image);

        return fetch(UPLOAD_RECIPE_IMAGE, {
            body: imageData,
            cache: 'no-cache',
            contentType: false,
            method: 'POST',
            processData: false
        })
            .then(response => response.json())
    }

    function onSubmit(event) {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = {};
        for (const key of formData.keys()) {
            data[key] = formData.get(key);
        }
        uploadRecipeImage()
            .then(uploadImageResponse => {
                data.INGREDIENTS = ingredients;
                data.INSTRUCTIONS = instructions;
                data.USRID = user.id;
                data.IMAGE = uploadImageResponse.id;
                submitRecipe(data)
                    .then(submitRecipeResponse => submitRecipeResponse.json())
                    .then(response => {
                        dispatch(changeAlert({ level: 'info', text: 'Recipe successfully created!' }));
                        history.push(RECIPE.replace(':id', response.RECIPEID));
                    })
                    .catch(() => dispatch(changeAlert({ level: 'error', text: 'Recipe could not be created!' })));
            });
    }

    return <form className="new-recipe-form" name="NEW_RECIPE" onSubmit={onSubmit}>
        <div className="left-side">
            <label htmlFor="NAME">Recipe name: </label>
            <input type="text" id="NAME" name="NAME" placeholder="Recipe name" required />

            <label htmlFor="TIME">Cook time: </label>
            <input type="number" id="TIME" name="TIME" min="0" placeholder="0" />

            <label htmlFor="SERVINGS">Servings: </label>
            <input type="number" id="SERVINGS" name="SERVINGS" min="0" placeholder="0" />

            <label htmlFor="CALORIES">Calories: </label>
            <input type="number" id="CALORIES" name="CALORIES" min="0" placeholder="0" />

            <label htmlFor="COURSE">Course: </label>
            <select id="COURSE" name="COURSE">
                <Suspense fallback={<option>Loading...</option>}>
                    <OptionsList url={COURSES} />
                </Suspense>
            </select>
        </div>
        <div className="right-side">
            <label htmlFor="IMAGE">Add image: </label>
            <Dropzone accept="image/*" style={{
                backgroundImage: `url(${image && image.preview})`,
                backgroundSize: 'contain',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center'
            }} id="IMAGE" name="IMAGE" onDrop={
                files => {
                    const imageFile = files.map(
                        file => Object.assign(file, {
                            preview: URL.createObjectURL(file)
                        })
                    )[0];
                    setImage(imageFile);
                }
            } multiple={false} className="dropzone-container" required></Dropzone>

            <label style={{visibility: 'hidden'}} htmlFor="EMPTY">Tags: </label>
            <input style={{visibility: 'hidden'}} id="EMPTY" type="text" />

            <label htmlFor="CUISINE">Cuisine: </label>
            <select id="CUISINE" name="CUISINE">
                <Suspense fallback={<option>Loading...</option>}>
                    <OptionsList url={CUISINES} />
                </Suspense>
            </select>
        </div>
        <div className="instructions-ingredients">
            <IngredientsList changeIngredients={setIngredients} />
            <InstructionsList changeInstructions={setInstructions} />
        </div>
        <input type="submit" value="Submit recipe" />
    </form>;
};

export default withRouter(connect(
    ({ user }) => ({ user })
)(NewRecipeForm));