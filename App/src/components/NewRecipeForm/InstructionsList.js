import React, { useState, useRef } from 'react';
import { ENDPOINTS } from 'utils/constants';
import styled from 'styled-components';

const { REMOVE } = ENDPOINTS.IMAGES;
const RemoveItem = styled.div`
    background-image: url(${props => props.image});
    width: ${props => props.size}vw;
    height: ${props => props.size}vw;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    float: left;

    &:hover {
        cursor: pointer;
    }
`;

const Instruction = ({ id, text, onRemove }) => <div className="instruction-container">
    <div className="instruction" type="text" placeholder="Instruction">{id + 1}. {text}</div>
    <RemoveItem image={REMOVE} onClick={onRemove} size={1} />
</div>;

const generateInstructions = (instructions, onRemove) => instructions.map(
    (text, index) => <Instruction id={index} key={index} text={text} onRemove={onRemove(index)} />
);

const InstructionsList = ({ changeInstructions }) => {
    const [instructions, setInstructions] = useState([]);
    const instructionInput = useRef(null);
    const addInstruction = value => {
        if (!instructionInput.current.value) {
            return;
        }
        instructionInput.current.value = '';
        changeInstructions([...instructions, value])
        setInstructions([...instructions, value]);
    };
    const onClick = () => {
        const { value } = instructionInput.current;
        addInstruction(value);
    };
    const onKeyDown = event => {
        if (event.keyCode === 13) {
            event.preventDefault();
            addInstruction(event.target.value);
        }
    };
    const removeInstruction = index => () => {
        instructions.splice(index, 1);
        changeInstructions([...instructions]);
        setInstructions([...instructions]);
    };
    return <div className="instructions">
        <label>Instructions</label>
        {generateInstructions(instructions, removeInstruction)}
        <div className="add-instruction">
            <input ref={instructionInput} onKeyDown={onKeyDown} id="new-instruction" type="text" placeholder="Instruction" required={instructions.length === 0} />
            <input type="button" value="Add" onClick={onClick} />
        </div>
    </div>;
};

export default InstructionsList;