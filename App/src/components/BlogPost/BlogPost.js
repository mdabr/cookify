import React from 'react';
import styled from 'styled-components';

const BlogImage = styled.img`
    float: left;
    width: 20vw;
    height: 20vw;
    margin-left: 5vw;
    margin-right: 2vw;
    background-color: #666;
`;

const BlogText = styled.p`
    color: white;
    width: 90%;
    margin-top: 0;
`;

const BlogPost = ({ image, blogObj }) => {
    const { description } = blogObj;
    return <div className="blog-post-inner-container">
        <BlogImage src={image} />
        <BlogText>{description}</BlogText>
    </div>;
};

export default BlogPost;