import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ROUTES, ENDPOINTS } from 'utils/constants';

const { CATEGORY } = ROUTES.MAIN_PAGE;
const CATEGORY_IMAGE = ENDPOINTS.IMAGES.CATEGORY;

const CategoryItemContainer = styled.div`
    background-image: url(${props => props.image});
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: 16vw;
    height: 10vw;
    margin: 2%;
    background-color: rgba(0, 0, 0, 0.3);
    float: left;
    position: relative;
    overflow: hidden;
    border-radius: 10px;

    &:hover {
        cursor: pointer;
    }
`;

const Title = styled.span`
    color: white;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 20%;
    background-color: rgba(0, 0, 0, 0.8);
`;

const CategoryItem = ({ category }) => {
    const { id, name } = category;
    return <Link to={{
        pathname: CATEGORY.replace(':id', id),
        categoryObj: category
    }}>
        <CategoryItemContainer image={CATEGORY_IMAGE.replace(':name', name.toLowerCase())}>
            <Title>{name}</Title>
        </CategoryItemContainer>
    </Link>;
};

export default CategoryItem;