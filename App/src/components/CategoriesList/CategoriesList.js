import React, { useEffect, useState } from 'react';
import './CategoriesList.css';
import CategoryItem from './CategoryItem/CategoryItem';
import { ENDPOINTS } from 'utils/constants';
import { NothingHereYet } from 'components';
import Loader from 'react-loader-spinner';

const { CATEGORIES } = ENDPOINTS;

const generateCategoryItems = categories => categories.map(
    (category, index) => <CategoryItem id={index} link={`/main-page/category/${index}`} key={index} category={category} />
);

const CategoriesList = () => {
    const [categories, setCategories] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchCategories = async () => {
            setIsFetching(true);

            try {
                const { categories: cats } = await fetch(CATEGORIES).then(response => response.json());
                setCategories(cats);
            } catch (error) {
                setCategories([]);
            } finally {
                setIsFetching(false);
            }

            setIsFetching(false);
        };

        fetchCategories();
    }, []);

    if (isFetching) {
        return <Loader type="ThreeDots" color="#fff" height="300" width="300" />;
    }

    return <div className="categories-list-container">
        {categories.length ? generateCategoryItems(categories) : <NothingHereYet />}
    </div>;
};

export default CategoriesList;