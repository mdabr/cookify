import React from 'react';
import './NewBlogPostForm.css';
import Dropzone from 'react-dropzone';

const NewBlogPostForm = () => <form className="new-blog-post-form">
    <div className="title-container">
        <label htmlFor="TITLE">Title: </label>
        <input type="text" id="TITLE" name="TITLE" />
    </div>

    <div className="image-container">
        <label htmlFor="IMAGE">Image: </label>
        <Dropzone className="dropzone-container" />
    </div>

    <div className="content-container">
        <label htmlFor="CONTENT">Content: </label>
        <textarea maxLength="200" placeholder="Place for your content (200 characters max)" />
    </div>

    <input type="submit" value="Add post" />
</form>;

export default NewBlogPostForm;