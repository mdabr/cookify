import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ENDPOINTS } from 'utils/constants';

const { RECIPE } = ENDPOINTS.IMAGES;

const RecipeItemContainer = styled.div`
    width: 40%;
    height: 50%;
    margin: 3% 5%;
    float: left;
    background-color: rgba(0,0,0,0.7);
    background-image: url(${props => props.image});
    background-size: contain;
    background-repeat: no-repeat;
    background-position: left;

    &:hover {
        cursor: pointer;
    }
`;
const RecipeDescription = styled.div`
    width: 50%;
    height: 100%;
    color: white;
    float: right;
    background-color: rgba(0, 0, 0, 1);
    overflow: hidden;

    &:hover {
        cursor: pointer;
    }
`;
const Title = styled.h4`
    margin: 11% auto;
`;
const LabelContainer = styled.div`
    width: 90%;
    margin: 12% auto;
`;

const RecipeItem = ({ title, cooking_time, calories, servings, image, id }) => {
    const recipeObj = {
        title,
        id
    };
    return <Link to={{
        pathname: `/main-page/recipe/${id}`,
        recipeObj
    }}>
        <RecipeItemContainer image={RECIPE.replace(':id', image)}>
            <RecipeDescription>
                <Title>{title}</Title>
                {cooking_time ? <LabelContainer>
                    <label htmlFor="cooking-time">Cooking time: </label>
                    <span id="cooking-time">{cooking_time}</span>
                </LabelContainer> : ''}
                {calories ? <LabelContainer>
                    <label htmlFor="calories">Calories: </label>
                    <span id="calories">{calories}</span>
                </LabelContainer> : ''}
                {servings ? <LabelContainer>
                    <label htmlFor="servings">Servings: </label>
                    <span id="servings">{servings}</span>
                </LabelContainer> : ''}
            </RecipeDescription>
        </RecipeItemContainer>
    </Link>;
};

export default RecipeItem;