import React, { useState, useEffect } from 'react';
import RecipeItem from './RecipeItem/RecipeItem';
import './RecipeList.css';
import { NothingHereYet } from 'components';
import { ENDPOINTS } from 'utils/constants';
import Loader from 'react-loader-spinner';

const { RECIPES } = ENDPOINTS;

const generateRecipes = (categoryId, categoryType) => recipes => recipes.map(
    (recipe, index) => <RecipeItem link={`/main-page/recipe/${recipe.id}`} {...recipe} key={index} id={recipe.id} categoryId={categoryId} categoryType={categoryType} />
);

const RecipeList = ({ categoryId, categoryType }) => {
    const [recipes, setRecipes] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchCategories = async () => {
            setIsFetching(true);

            try {
                setRecipes(
                    await fetch(
                        RECIPES.replace(':id', categoryId).replace(':type', categoryType)
                    )
                );
            } catch (error) {
                setRecipes([]);
            } finally {
                setIsFetching(false);
            }

            setIsFetching(false);
        };

        fetchCategories();
    }, []);

    if (isFetching) {
        return <Loader type="ThreeDots" color="#fff" height="300" width="300" />;
    }

    return <div className="recipe-list-container">
        {recipes.length ? generateRecipes(categoryId, categoryType)(recipes) : <NothingHereYet />}
    </div>;
};

export default RecipeList;