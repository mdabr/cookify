import React, { useEffect, useState } from 'react';
import RecipeItem from './RecipeItem/RecipeItem';
import './RecipeList.css';
import { NothingHereYet } from 'components';
import { ENDPOINTS } from 'utils/constants';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';

const { FAVOURITES } = ENDPOINTS;

const generateRecipes = recipes => recipes.map(
    (recipe, index) => <RecipeItem link={`/main-page/recipe/${recipe.id}`} {...recipe} key={index} id={recipe.id} />
);

const RecipeList = ({ user }) => {
    const [recipes, setRecipes] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchCategories = async () => {
            setIsFetching(true);

            try {
                setRecipes(
                    await fetch(
                        FAVOURITES.replace(':userid', user.id)
                    )
                );
            } catch (error) {
                setRecipes([]);
            } finally {
                setIsFetching(false);
            }

            setIsFetching(false);
        };

        fetchCategories();
    }, []);

    if (isFetching) {
        return <Loader type="ThreeDots" color="#fff" height="300" width="300" />;
    }

    return <div className="recipe-list-container">
        {recipes.length ? generateRecipes(recipes) : <NothingHereYet />}
    </div>;
};

export default connect(
    ({ user }) => ({ user })
)(RecipeList);