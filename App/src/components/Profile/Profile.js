import React from 'react';
import { ROUTES } from 'utils/constants';
import { connect } from 'react-redux';
import { resetUser } from 'store/actions';
import './Profile.css';
import { Link } from 'react-router-dom';

const { LOGIN } = ROUTES.PRELOGIN;

const Profile = ({ user, dispatch }) => {
    const { name } = user;
    const logout = () => {
        window.sessionStorage.removeItem('user');
        dispatch(resetUser());
    };
    return <div className="profile-container">
        <div className="profile-name">{name}</div>
        <div className="profile-logout">
            <Link to={LOGIN} onClick={logout}>Log out</Link>
        </div>
    </div>;
};

export default connect(
    ({ user }) => ({ user })
)(Profile);
