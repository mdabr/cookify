import React from 'react';
import './BottomBar.css';
import { RightPanel, LeftPanel } from 'components';

const BottomBar = () => <div className="bottom-bar-container">
    <LeftPanel />
    <RightPanel />
</div>;

export default BottomBar;
