import React, { useState, useEffect } from 'react';
import Slider from 'react-slick';
import { PanelElement } from 'components';
import { ENDPOINTS, ROUTES } from 'utils/constants';
import Loader from 'react-loader-spinner';

const { SLIDER } = ENDPOINTS;
const { RECIPE } = ROUTES.MAIN_PAGE;
const RECIPE_IMG = ENDPOINTS.IMAGES.RECIPE;

const generateSliderElements = elements => elements.map(({ id, title, image }, index) => {
    const recipeObj = {
        id,
        title,
        image
    };
    return <PanelElement title={title} image={RECIPE_IMG.replace(':id', image)} link={{
        pathname: RECIPE.replace(':id', id),
        recipeObj
    }} key={index} />;
});

const Slides = ({ settings }) => {
    const [recipes, setRecipes] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchRecipes = async () => {
            setIsFetching(true);

            try {
                setRecipes(
                    await fetch(SLIDER).then(response => response.json())
                );
            } catch (error) {
                setRecipes([]);
            } finally {
                setIsFetching(false);
            }

            setIsFetching(false);
        };

        fetchRecipes();
    }, []);

    if (isFetching) {
        return <div className="loader"><Loader type="ThreeDots" color="#fff" height="300" width="300" /></div>;
    }

    return <Slider {...settings} slidesToShow={Math.max(1, Math.min(recipes.length - 1, 3))}>{generateSliderElements(recipes)}</Slider>;
};

export default Slides;