import React from 'react';
import './LeftPanel.css';
import 'slick-carousel/slick/slick.css'; 
import 'slick-carousel/slick/slick-theme.css';

import Slides from './Slides';

const LeftPanel = () => {
    const settings = {
        arrows: true,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return <div className="left-side">
        <div className="slider-container">
            <Slides settings={settings} />
        </div>
    </div>;
};

export default LeftPanel;