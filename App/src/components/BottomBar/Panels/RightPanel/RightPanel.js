import React from 'react';
import './RightPanel.css';
import { PanelElement } from 'components';
import { ROUTES, ENDPOINTS } from 'utils/constants';

const { BLOG, NEW_RECIPE, SAVED_RECIPES, CATEGORIES } = ROUTES.MAIN_PAGE;
const createPanelElement = ({ title, image, link }, index) => <PanelElement title={title} image={image} link={link} key={index} />;
const panelElements = [
    {
        title: 'Blog',
        image: ENDPOINTS.IMAGES.BLOG,
        link: BLOG
    },
    {
        title: 'New recipe',
        image: ENDPOINTS.IMAGES.NEW_RECIPE,
        link: NEW_RECIPE
    },
    {
        title: 'Saved recipes',
        image: ENDPOINTS.IMAGES.SAVED_RECIPES,
        link: SAVED_RECIPES
    },
    {
        title: 'Categories',
        image: ENDPOINTS.IMAGES.CATEGORIES,
        link: CATEGORIES
    }
];
const RightPanel = () => <div className="right-side">{panelElements.map(createPanelElement)}</div>;

export default RightPanel;