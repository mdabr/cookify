import React from 'react';
import './PanelElement.css';

import { Link } from 'react-router-dom';

const PanelElement = ({ title, image, link }) => {
    return <Link to={link}>
        <div className="panel-element-container">
            <div className="panel-element-image" style={{ backgroundImage: `url(${image})` }}></div>
            <div className="panel-element-title">{title}</div>
        </div>
    </Link>;
};

export default PanelElement;