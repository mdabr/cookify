import React, { useRef } from 'react';
import './RegisterForm.css';
import { ROUTES } from 'utils/constants';
import { Link } from 'react-router-dom';
import { submitRegistration } from 'utils/FormService';
import { connect } from 'react-redux';
import { changeAlert } from 'store/actions';
import { getErrorMessage, isError } from 'utils/errors';

const { LOGIN } = ROUTES.PRELOGIN;
const clearInput = input => {
    input.current.value = '';
};
const RegisterForm = ({ dispatch }) => {
    const nameInput = useRef(null);
    const emailInput = useRef(null);
    const passInput = useRef(null);
    const passConfInput = useRef(null);

    const validatePassword = () => {
        const passwordsMatch = passInput.current.value === passConfInput.current.value;
        const customValidity = passwordsMatch ? '' : 'Passwords must match!';
        passConfInput.current.setCustomValidity(customValidity);
    };

    const onSubmit = event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const data = {};
        for (const key of formData.keys()) {
            data[key] = formData.get(key);
        }
        submitRegistration(data)
            .then(response => {
                if (isError(response.status)) {
                    throw response;
                }
                [nameInput, emailInput, passInput, passConfInput].forEach(clearInput);
                dispatch(changeAlert({ text: 'User successfully registered!', level: 'info' }));
            })
            .catch(response => response.json()
                .then(({ error }) => dispatch(changeAlert({ text: getErrorMessage(error.number), level: 'error' })))
                .catch(() => dispatch(changeAlert({ text: getErrorMessage(), level: 'error' })))
            );
    };

    return <form onSubmit={onSubmit} className="register-form">
        <h4>Registration</h4>
        <label htmlFor="NAME">Name</label>
        <input id="NAME" name="NAME" ref={nameInput} type="text" required />

        <label htmlFor="EMAIL">Email</label>
        <input id="EMAIL" name="EMAIL" ref={emailInput} type="email" required />

        <label htmlFor="PASSWORD">Password</label>
        <input id="PASSWORD" name="PASSWORD" ref={passInput} onChange={validatePassword} type="password" required />

        <label htmlFor="PASSWORDCONFIRM">Confirm password</label>
        <input id="PASSWORDCONFIRM" name="PASSWORDCONFIRM" ref={passConfInput} onKeyUp={validatePassword} type="password" required />
        <Link to={LOGIN}>
            <input type="button" value="&#171; Go back" />
        </Link>
        <input type="submit" value="Register" />
    </form>;
};

export default connect()(RegisterForm);
