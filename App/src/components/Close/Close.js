import React from 'react';
import { Link } from 'react-router-dom';
import './Close.css';
import styled from 'styled-components';

const CloseContainer = styled.div`
    background-image: url(${props => props.image});
    width: ${props => props.size}vh;
    height: ${props => props.size}vh;
`;

const Close = ({ link, image, size }) => <Link to={link}>
    <CloseContainer className="close" image={image} size={size} />
</Link>;

export default Close;