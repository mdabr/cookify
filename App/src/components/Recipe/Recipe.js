import React, { useState, useEffect } from 'react';
import './Recipe.css';
import styled from 'styled-components';
import { ENDPOINTS } from 'utils/constants';
import Loader from 'react-loader-spinner';

const Container = styled.div`
    position: relative;
    margin: 2vw;
    height: 90%;
    overflow: scroll;
`;
const Image = styled.div`
    background-image: url(${props => props.image});
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    border-radius: 10px;
    width: 20vw;
    height: 20vw;
    background-color: rgba(0,0,0,0.8);
    float: left;
`;
const IngredientsContainer = styled.div`
    float: left;
    color: white;
    width: 33%;
`;
const IngredientsList = styled.ul`
    text-align: left;
    overflow: scroll;
    height: 16vw;
`;
const Ingredient = styled.li``;
const Description = styled.div`
    width: 29%;
    height: 15vw;
    float: left;
    color: white;
    margin: 5% 2%;
    border: 1px solid #666;
`;
const Title = styled.h4`
    color: white;
    margin: 1%;
`;
const Instructions = styled.ol`
    color: white;
`;
const Instruction = styled.li`
    text-align: left;
`;

const { RECIPE } = ENDPOINTS;
const RECIPE_IMG = ENDPOINTS.IMAGES.RECIPE;

const Recipe = ({ id }) => {
    const [recipe, setRecipe] = useState(null);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchCategories = async () => {
            setIsFetching(true);

            try {
                setRecipe(
                    await fetch(RECIPE.replace(':id', id))
                );
            } catch (error) {
                // TODO: add component FailedToFetch when recipe couldn't be fetched
                setRecipe(null);
            } finally {
                setIsFetching(false);
            }

            setIsFetching(false);
        };

        fetchCategories();
    }, []);

    if (isFetching || !recipe) {
        return <Loader type="ThreeDots" color="#fff" height="300" width="300" />;
    }

    const { image, ingredients, cooking_time, servings, calories, course, cuisine, instructions } = recipe;

    return <Container>
        <Image image={RECIPE_IMG.replace(':id', image)} />
        <IngredientsContainer>
            <Title>Ingredients: </Title>
            <IngredientsList>
                {ingredients.map(
                    ({ amount, unit, name }, index) => <Ingredient key={index}>{amount} {unit} {name}</Ingredient>
                )}
            </IngredientsList>
        </IngredientsContainer>
        <Description>
            <p>Cooking time: {cooking_time} {cooking_time > 1 ? 'minutes' : 'minute'}</p>
            <p>Servings: {servings} {servings > 1 ? 'people' : 'person'}</p>
            <p>Calories: {calories} kcal</p>
            <p>Course: {course.name}</p>
            <p>Cuisine: {cuisine.name}</p>
        </Description>
        <Instructions>
            <Title>Instructions: </Title>
            {instructions.map(
                ({ content }, index) => <Instruction key={index}>{content}</Instruction>
            )}
        </Instructions>
    </Container>;
};

export default Recipe;