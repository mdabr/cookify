import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { resetAlert } from 'store/actions';

const AlertContainer = styled.div`
    background-color: ${props => props.color};
    position: absolute;
    right: 0;
    left: 0;
    bottom: ${props => props.show ? '10vh' : '-50vh'};
    width: fit-content;
    padding: 1%;
    -webkit-transition: all 1000ms linear;
    transition: all 1000ms linear;
    border: 1px solid #000;
    border-radius: 10px;
    margin: auto;
`;

const mapLevelToColor = level => {
    const colorsMap = {
        'info': 'green',
        'warn': 'yellow',
        'error': 'red'
    };
    return colorsMap[level] || 'transparent';
};

const Infobox = ({ alert, dispatch }) => {
    const [show, setShow] = useState(false);
    const { text, level } = alert;
    useEffect(() => {
        if (text) {
            setShow(true);
            setTimeout(() => {
                dispatch(resetAlert());
                setShow(false);
            }, 5000);
        }
    }, [text]);
    return <AlertContainer show={show} color={mapLevelToColor(level)}>{text}</AlertContainer>;
};

export default connect(
    ({ alert }) => ({ alert })
)(Infobox);