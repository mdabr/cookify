import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { PreLoginPage, MainPage, NotFound } from 'views';
import { ROUTES } from 'utils/constants';
import { Infobox } from 'components';
import { connect } from 'react-redux';
import { changeUser } from 'store/actions';

const { BASE, TITLE_PAGE } = ROUTES.MAIN_PAGE;
const { LOGIN } = ROUTES.PRELOGIN;

const App = ({ user, dispatch }) => {
    const storedUser = JSON.parse(window.sessionStorage.getItem('user'));
    const { id } = storedUser || user;
    if (!user.id && storedUser && storedUser.id) {
        dispatch(changeUser(storedUser));
    }
    return <Router>
        <>
            <Switch>
                <Route exact path="/" component={() => id ? <Redirect to={TITLE_PAGE} /> : <Redirect to={LOGIN} />} />
                <Route exact path={BASE} component={() => <Redirect to={TITLE_PAGE} />} />
                <Route path={LOGIN} component={() => id ? <Redirect to={TITLE_PAGE} /> : <PreLoginPage />} />
                <Route path={BASE} component={() => id ? <MainPage /> : <Redirect to={LOGIN} />} />
                <Route component={() => <NotFound />} />
            </Switch>
            <Infobox />
        </>
    </Router>;
};

export default connect(
    ({ user }) => ({ user })
)(App);
